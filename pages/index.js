import { Table } from "../components/table";
import React from 'react';
import ReactDOM from 'react-dom/client';
import BacklinkNav from "@/components/backlink_nav";

export default function Home({Component, ...rest}) {
	return (
		<>
			<div className="bg-grayy3">
				<BacklinkNav/>
				<Table />
			</div>
			<script src="https://code.iconify.design/iconify-icon/1.0.1/iconify-icon.min.js"></script>
		</>
	);
}
