import { MainForm } from "@/components/main_form";
import React, { useEffect, useState } from "react"; // import state
import { Provider } from 'react-redux'
import { wrapper } from '../store'

export default function Create({Component, ...rest}) {
    const { store, props } = wrapper.useWrappedStore(rest)
    return (
      <>
        <Provider store={store}>
            <div className="bg-grayy3">
                <MainForm/>
            </div>
        </Provider>
        <script src="https://code.iconify.design/iconify-icon/1.0.1/iconify-icon.min.js"></script>
      </>
      )
  }