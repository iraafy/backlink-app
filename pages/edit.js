import { UpdateForm } from "@/components/update_form";
import React, { useEffect, useState } from "react"; // import state
import { Provider } from 'react-redux'
import { wrapper } from '../store'

export default function Edit({Component, ...rest}) {
    const { store, props } = wrapper.useWrappedStore(rest)
    return (
      <>
        <Provider store={store}>
            <div className="bg-grayy3">
                <UpdateForm />
            </div>
        </Provider>
        <script src="https://code.iconify.design/iconify-icon/1.0.1/iconify-icon.min.js"></script>
      </>
      )
  }