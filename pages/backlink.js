export default function Backlink({Component, ...rest}) {
    return (
      <>
        <div className="bg-grayy3">
            <div className="w-full flex flex-wrap"> 
                <div className="w-full lg:w-2/3 p-5">
                    <div className="w-full container">                        
                        <nav className="flex mb-7" aria-label="Breadcrumb">
                            <ol className="inline-flex items-center space-x-1 md:space-x-3">
                                <li className="inline-flex items-center">
                                    <a href="#" className="inline-flex items-center text-sm font-medium text-gray-900">
                                        Publication
                                    </a>
                                </li>
                                <li>
                                    <div className="flex items-center">
                                        <iconify-icon inline icon="mdi:slash-forward" className="text-gray-400"></iconify-icon>
                                        <span className="ml-1 text-sm font-medium text-red md:ml-2">Backlink</span>
                                    </div>
                                </li>
                            </ol>
                        </nav>
                    </div>    
                </div>
            </div>
            <div className="w-full flex flex-wrap"> 
                <div className="w-full lg:w-1/2 pl-5 pr-5">
                    <div className="flex items-center mb-7">
                        <label for="twitter" className="text-black text-sm font-medium inline-block text-left mr-4">Show</label>
                        <select className="w-70 border p-2 rounded-md text-sm font-medium">
                            <option value="10">10</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <p className="mx-3">entries</p>
                        <form>   
                            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                            <div class="relative">
                                <input type="search" id="default-search" class="bg-white border border-gray-300 text-sm rounded-md block p-2" placeholder="Search Data" required/>
                                <button type="submit" class="text-white absolute right-0 bottom-0 rounded-md text-sm px-4 py-2 bg-red">Search</button>
                            </div>
                        </form>
                        <button type="submit" class="text-white rounded-md text-sm px-4 py-2 bg-red mx-3">Show Filter <iconify-icon inline icon="iwwa:sort-by"></iconify-icon></button>
                    </div>
                </div>
                <div className="w-full lg:w-1/2 px-5 text-white">
                    <div className="flex justify-end items-end">
                        <button type="submit" class="rounded-md text-sm px-4 py-2 bg-red mx-3"><iconify-icon inline icon="tabler:logout"></iconify-icon> Export CSV</button>
                        <button type="submit" class="text-white rounded-md text-sm px-4 py-2 bg-red"><iconify-icon inline icon="material-symbols:add"></iconify-icon> Create</button>
                    </div>
                </div>
            </div>
          </div>
      </>
      )
  }