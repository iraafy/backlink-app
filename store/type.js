//Action Types Article
export const GET_ARTICLE_BY_ID = "article/GET_ARTICLE_BY_ID";

//Action Types Language
export const GET_LANGUAGE = "language/GET_LANGUAGE";
export const SET_LANGUAGE = "language/SET_LANGUAGE";
