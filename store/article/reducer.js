import { GET_ARTICLE_BY_ID } from "../type";

const articleReducer = (state = 0, action) => {
  switch (action.type) {
    case GET_ARTICLE_BY_ID:
      return action.payload;
    default:
      return state;
  }
};

export default articleReducer;
