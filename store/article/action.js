import { GET_ARTICLE_BY_ID } from "../type";

export const getArticleById = (data) => ({
  type: GET_ARTICLE_BY_ID,
  payload: data,
});
