import { GET_LANGUAGE, SET_LANGUAGE } from "../type";

const languageReducer = (state = "id", action) => {
  switch (action.type) {
    case GET_LANGUAGE:
      return action.payload;
    case SET_LANGUAGE:
      return action.payload;
    default:
      return state;
  }
};

export default languageReducer;
