import { GET_LANGUAGE, SET_LANGUAGE } from "../type";

export const getCurrentLanguage = (data) => ({
  type: GET_LANGUAGE,
  payload: data,
});

export const setLanguage = (data) => ({
  type: SET_LANGUAGE,
  payload: data,
});
