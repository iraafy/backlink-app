import articleReducer from "./article/reducer";
import languageReducer from "./language/reducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  article: articleReducer,
  language: languageReducer,
});

export default rootReducer;
