/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin'); 

module.exports = {
  mode: 'jit',
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,html}",
    "./components/**/*.{js,ts,jsx,tsx,html}",
  ],
  theme: {
    extend: {
      colors: {
        'red': '#EB0000',
        'grayy' : '#CFCFCF',
        'grayy2' : '#A6A6A6',
        'grayy3' : '#F5F5F5',
        'red1' : '#DE193E',
      },
      borderRadius: {
        'sm' : '0.4rem',
        'md' : '0.5rem',
        'lg' : '1rem',
      },
    },
    plugins: [
      plugin(function({ addVariant }) {
              addVariant('current', '&.active');
          })
    ],
  },
  // plugins: [
  //   plugin(function ({ addUtilities }) {
  //     const utilities = {
  //         ".bg-hero": {
  //         "background-image": "url('/images/BG.png')",
  //       }
  //     };
  //     addUtilities(utilities);
  //   })
  // ],
}
