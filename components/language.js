import { useDispatch, useSelector } from 'react-redux'
import { setLanguage } from '@/store/language/action'
import { currentLanguage } from '@/lib/language'
import React, { useState, useEffect } from 'react'
import cookies from 'cookie-cutter'

export const Language = () => {
    let language = useSelector(state => state.language)
    const dispatch = useDispatch()
    const [openTab, setOpenTab] = React.useState(1);

    // const uploadToServer = async (event) => {
    //     const body = new FormData();
    //     body.append("file", image);
    //     const response = await fetch("/api/file", {
    //       method: "POST",
    //       body
    //     });

    const changeLanguage = e => {
        dispatch(setLanguage(e.target.value))
        localStorage.setItem('language', e.target.value)
        cookies.set('language', e.target.value)
    }

    useEffect(() => {
        if (localStorage.getItem('language')) {
            dispatch(setLanguage(localStorage.getItem('language')))
            cookies.set('language', localStorage.getItem('language'))
        } else {
            dispatch(setLanguage('id'))
            localStorage.setItem('language', 'id')
            cookies.set('language', 'id')
        }
    })
    return (
        <>
            <div className="hidden w-full md:block md:w-auto">
                <ul className="flex flex-col mt-4 md:flex-row md:mt-0 md:text-sm md:font-medium md:border-0">
                    <li>
                        <div className={"py-0.5 pl-0.5 pr-1 bg-grayy3 text-left font-bold rounded-t " +
                            (openTab === 1
                                ? " text-white bg-red"
                                : "text-black bg-transparent"
                            )}
                            value={language}
                            onClick={e => {
                                e.preventDefault()
                                setOpenTab(1);
                            }}
                            data-toggle="tab"
                            href="#lang1"
                            role="tablist">
                            <button onClick={changeLanguage} value="id" className="py-3 pl-2 pr-4 text-sm font-medium">Indonesia (id)</button>
                        </div>
                    </li>
                    <li>
                        <div className={"py-0.5 pl-0.5 pr-1 bg-grayy3 text-left text-black font-bold rounded-t " +
                            (openTab === 2
                                ? "text-white bg-red"
                                : "text-black bg-transparent")
                        }
                            value={language}
                            onClick={e => {
                                e.preventDefault()
                                setOpenTab(2);
                            }}
                            data-toggle="tab"
                            href="#lang2"
                            role="tablist"
                        >
                            <button onClick={changeLanguage} value="en" className="py-3 pl-2 pr-4 text-sm font-medium">English(En)</button></div>
                    </li>
                    <li>
                    <div className={"py-0.5 pl-0.5 pr-1 bg-grayy3 text-left text-black font-bold rounded-t " +
                            (openTab === 3
                                ? "text-white bg-red"
                                : "text-black bg-transparent")
                        }
                            value={language}
                            onClick={e => {
                                e.preventDefault()
                                setOpenTab(3);
                            }}
                            data-toggle="tab"
                            href="#lang3"
                            role="tablist"
                        >
                            <button onClick={changeLanguage} value="cn" className="py-3 pl-2 pr-4 text-sm font-medium">Chinese(中国人)</button></div>
                    </li>
                </ul>
                <div className="flex-grow border-t border-gray-300"></div>
            </div>
        </>
    )
}