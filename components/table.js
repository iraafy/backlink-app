import { useRouter } from "next/router";
import React, { useEffect, useState } from "react"; // import state
import axios from "axios";
import Swal from 'sweetalert2'

import Link from 'next/link'

export const Table = () => {
    const [modal, setModal] = useState(false);
    const [loop, setLoop] = useState(true);
    const toggleModal = () => {
        setModal(!modal)
    }
    const [content, setContent] = useState([]);
    const router = useRouter();
    const {query: {limit}} = router
    const {query: {title}} = router
    const {query: {platform}} = router
    const {query: {position}} = router
    const {query: {status}} = router
    // const {query: {position}} = router


	useEffect(() => {
		getContents()
        console.log(title)
	}, [])

	const getContents = () => {
        axios.get('https://backlink-service.herokuapp.com/api/v2/backlinks')
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                    console.log(err.data);
            })
        }

    if(platform && limit && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?platform=${platform}&limit=${limit}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }
    else if(position && limit && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?position=${position}&limit=${limit}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }
    else if(status && limit && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?status=${status}&limit=${limit}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }
    else if(title && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?title=${title}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }
    else if(platform && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?platform=${platform}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }
    else if(position && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?position=${position}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }
    else if(status && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?status=${status}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }
    else if(limit && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks?limit=${limit}`)
            .then((res) => {
                setContent(res.data.data)
            })
            .catch((err) => {
                console.log(err.data);
            })
        setLoop(false)
    }

	const postDelete = (id, e) => {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "Items that you delete will not be able to be restored again",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`https://backlink-service.herokuapp.com/api/v2/backlinks/${id}`)
                .then((res) => {
                    console.log('deleted', res);
                    router.reload();
                })
                .catch((err) => {
                    console.log(err.data);
                })
                axios.get('https://backlink-service.herokuapp.com/api/v2/backlinks')
                .then((res) => {
                    setContent(res.data.data)
                    router.reload();
                })
                .catch((err) => {
                    console.log(err.data);
                })
            }
          })
            // window.location.reload();
	}

    function sendProps(id){
        router.push({
            pathname: '/create',
            query: {
                id
            }
        })
    }
  return (
    <>
        <div className="m-5 mt-0 overflow-x-auto relative">
            <table className="w-full text-sm text-white">
                <thead className="text-xs bg-red text-left">
                    <tr>
                        <th scope="col" className="py-3 px-6">
                            Title
                        </th>
                        <th scope="col" className="py-3 px-6">
                            Platform
                        </th>
                        <th scope="col" className="py-3 px-6">
                            Position
                        </th>
                        <th scope="col" className="py-3 px-6">
                            Start Date
                        </th>
                        <th scope="col" className="py-3 px-6">
                            End Date
                        </th>
                        <th scope="col" className="py-3 px-6">
                            Shown
                        </th>
                        <th scope="col" className="py-3 px-6">
                            Clicked
                        </th>
                        <th scope="col" className="py-3 px-6">
                            CTR
                        </th>
                        <th scope="col" className="py-3 px-6">
                            Status
                        </th>
                        <th scope="col" className="py-3 px-6">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody className="text-left">
                    {content?.map((cont) => {
                        return (
                            <>
                            <tr className="bg-white border-b text-black">
                                <th scope="row" className="py-4 px-6 font-medium">
                                    <p>
                                        {cont.title}
                                    </p>
                                </th>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.platform}
                                    </p>
                                </td>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.position}
                                    </p>
                                </td>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.start_date}
                                    </p>
                                </td>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.end_date}
                                    </p>
                                </td>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.shown}
                                    </p>
                                </td>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.clicked}
                                    </p>
                                </td>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.ctr}
                                    </p>
                                </td>
                                <td className="py-4 px-6">
                                    <p>
                                        {cont.backlink.status}
                                    </p>
                                </td>
                                <button onClick={() =>sendProps(cont.backlink.id)} className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-sm text-sm px-3 py-2 my-2 mr-1">
                                    <iconify-icon inline icon="material-symbols:edit-square-outline-sharp"></iconify-icon>
                                </button>
                                <button onClick={(e) => postDelete(cont.backlink.id, e)} className="focus:cursor-wait cursor-pointer focus:outline-none text-white bg-red focus:ring-4 font-medium rounded-sm text-sm px-3 py-2 my-2">
                                    <iconify-icon inline icon="material-symbols:delete-outline-rounded"></iconify-icon>
                                </button>
                                {/* <button className="focus:cursor-wait cursor-pointer focus:outline-none text-white bg-red focus:ring-4 font-medium rounded-sm text-sm px-3 py-2 my-2" type="button" 
                                    onClick={toggleModal}>
                                    <iconify-icon inline icon="material-symbols:delete-outline-rounded"></iconify-icon>
                                </button>
                                {modal ? (
                                    <div>
                                        tes-
                                        <button onClick={toggleModal}>
                                            close
                                        </button>
                                    </div>
                                ) : null} */}
                            </tr>
                        </> 
                        )
                    })}
                </tbody>
            </table>
        </div>
    </>
    );
};