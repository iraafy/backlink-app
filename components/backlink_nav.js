import React, { useEffect, useState } from "react"; // import state
import { useRouter } from "next/router";
import { Filter } from "./button_filter";
import { set } from "cookie-cutter";
import axios from "axios";


export default function BacklinkNav({Component, ...rest}) {
    const saya1 = 'saya1';
    const router = useRouter();
    const [button_filter, setFilter] = useState(false);
    const [reload, setReload] = useState(false);

    const [data, setData] = useState({
        title: "",
    })
    
    function handle(e) {
        const newData = {...data}
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)
    }

    function search(e){
        e.preventDefault()
        router.push({
            pathname: './',
            query: {
                title: data.title
            }
        }).then(() => router.reload())
    }

    function setLimit(e) {
        var limit = e.target.value
        e.preventDefault()
        router.push({
            pathname: './',
            query: {
                limit: limit
            }
        }).then(() => router.reload())
    }

    return (
    <>
        <div className="bg-grayy3">
            <div className="w-full flex flex-wrap"> 
                <div className="w-full lg:w-2/3 p-5">
                    <div className="w-full container">                        
                        <nav className="flex mb-7" aria-label="Breadcrumb">
                            <ol className="inline-flex items-center space-x-1 md:space-x-3">
                                <li className="inline-flex items-center">
                                    <a href="#" className="inline-flex items-center text-sm font-medium text-gray-900">
                                        Publication
                                    </a>
                                </li>
                                <li>
                                    <div className="flex items-center">
                                        <iconify-icon inline icon="mdi:slash-forward" className="text-gray-400"></iconify-icon>
                                        <span className="ml-1 text-sm font-medium text-red md:ml-2">Backlink</span>
                                    </div>
                                </li>
                            </ol>
                        </nav>
                    </div>    
                </div>
            </div>
            <div className="w-full lg:w-full lg:flex lg:flex-wrap"> 
                <div className="w-full lg:w-8/12 pl-5">
                    <div className="grid grid-cols-2 items-center lg:flex mb-7">
                        <label className="text-black text-sm font-medium inline-block text-left mr-4 pl-3 lg:pl-0">Show</label>
                        <select onChange={(e) => setLimit(e)} id="limit" className="w-70 border p-2 rounded-md text-sm font-medium">
                            <option value="">Select</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                        </select>
                        <p className="mx-3">entries</p>
                        <form onSubmit={search}>   
                            <label className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                            <div className="relative">
                                <input type="search" onChange={(e) => handle(e)} id="title" className="bg-white border border-gray-300 text-sm rounded-md block p-2" placeholder="Search Data" required/>
                                <button type="submit" className="text-white absolute right-0 bottom-0 rounded-md text-sm px-4 py-2 bg-red">Search</button>
                            </div>
                        </form>
                        <button type="submit" onClick={() => setFilter(!button_filter)} className="text-white rounded-md text-sm px-4 py-2 bg-red mx-3 lg:mt-0 mt-4 col-span-2">
                            Show Filter <iconify-icon inline icon="iwwa:sort-by"></iconify-icon>
                        </button>
                        <label className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Filter By</label>  
                    </div>
                    <div className="w-3/4 container bg-white absolute z-50 bg-white pl-5" style={{boxShadow: '0px 0px 7px 1px rgba(0,0,0,0.50)'}}>
                        {button_filter && <Filter setFilter={setFilter} />}
                    </div>
                </div>
                <div className="w-full lg:w-4/12 lg:px-5 lg:pb-0 pb-5 px-4 text-white">
                    <div className="lg:flex justify-end items-end">
                        <button type="submit" className="rounded-md text-sm px-4 py-2 bg-red mx-3"><iconify-icon inline icon="tabler:logout"></iconify-icon> Export CSV</button>
                        <a href="/create">
                            <button type="submit" className="text-white rounded-md text-sm px-4 py-2 bg-red"><iconify-icon inline icon="material-symbols:add"></iconify-icon> Create</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </>
    );
}