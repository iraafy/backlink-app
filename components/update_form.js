import {useRouter} from 'next/router';
import { useSelector } from 'react-redux'
import { useEffect, useState } from "react"
import { currentLanguage } from '@/lib/language'
import axios from "axios";
import { Language } from './language';
import { Edit } from './edit';
import Swal from 'sweetalert2';

export const UpdateForm = () => {

    let language = useSelector(state => state.language)
    const [image, setImage] = useState(null);
    const [edit, setEdit] = useState(false);
    const [createObjectURL, setCreateObjectURL] = useState(null);
    const router = useRouter();

    const [content, setContent] = useState([]);
	useEffect(() => {
		getContents()
	}, [])

    const uploadToClient = (event) => {
        if (event.target.files && event.target.files[0]){
            const i = event.target.files[0];
            setImage(i)
            setCreateObjectURL(URL.createObjectURL(i));
        }
    };

    const { id } = router.query

    const getContents = () => {
		axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks/${id}`)
			.then((res) => {
				setContent(res.data.data)
			})
			.catch((err) => {
				console.log(err.data);
			})
	}

    function postDelete(e) {
            
	}
    
    function handleEdit() {
        setEdit(true);
    }
    
    return (
    <>
        <div className="w-full p-5 pb-0">
            <nav className="flex mb-7" aria-label="Breadcrumb">
                <ol className="inline-flex items-center space-x-1 md:space-x-3">
                    <li className="inline-flex items-center">
                        <a href="#" className="inline-flex items-center text-sm font-medium text-gray-900">
                            Publication
                        </a>
                    </li>
                    <li>
                        <div className="flex items-center">
                            <iconify-icon inline icon="mdi:slash-forward" className="text-gray-400"></iconify-icon>
                            <a href="/" className="ml-1 text-sm font-medium hover:text-gray-900">Backlink</a>
                        </div>
                    </li>
                    <li aria-current="page">
                        <div className="flex items-center">
                            <iconify-icon inline icon="mdi:slash-forward" className="text-gray-400"></iconify-icon>
                            <span className="ml-1 text-sm font-medium text-red md:ml-2">Edit Backlink</span>
                        </div>
                    </li>
                </ol>
            </nav>
        </div>

        <p>
            {content?.map((item) => {
                return (
                    <>
                        {item.title}
                    </>
                )
            })}
        </p>
    </>
  )
}