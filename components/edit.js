import React from "react";
export const Edit = ({setEdit}) =>{
    function handleEdit() {
        setEdit(false);
    }

    return(
        <div className= "w-full bg-stone-300 ">
            <form action="" className="lg:w-2/3 p-5">
                <div className="flex items-center mb-5">
                    <label for="name" className="w-full text-black text-sm font-medium inline-block text-left ml-3 mt-5">Article URL<span className="text-red">*</span></label>
                    <span href="#" onClick={handleEdit} style={{cursor: 'pointer'}} className="w-full text-red text-sm font-medium inline-block text-right ml-3 mt-5">Cancel</span>
                </div>
                <input type="" className="mt-5 ml-3 bg-stone-500 border border-gray-300 text-sm w-full rounded-md block p-2.5" placeholder="htpss://"/>

                <div className="flex items-center mb-5">
                    <label for="name" className="w-full text-black text-sm font-medium inline-block text-left ml-3 mt-5">New Article URL<span className="text-red">*</span></label>
                    <label for="name" className="w-full text-black text-sm font-medium inline-block text-right ml-3 mt-5">64 char remaining 0</label>
                </div>
                <input type="" className="mt-5 ml-3 bg-white border border-gray-400 text-sm w-full rounded-md block p-2.5"/>
            </form>
        </div>
    )
}
