import {useRouter} from 'next/router';
import { useSelector } from 'react-redux'
import { useState } from "react"
import { currentLanguage } from '@/lib/language'
import axios from "axios";
import { Language } from './language';
import { Edit } from './edit';
import Swal from 'sweetalert2';

export const MainForm = () => {
    let language = useSelector(state => state.language)

    const [image, setImage] = useState(null);
    const [edit, setEdit] = useState(false);
    const [loop, setLoop] = useState(true);
    const [createObjectURL, setCreateObjectURL] = useState(null);
    const router = useRouter();
    const {query: {id}} = router

    const uploadToClient = (event) => {
        if (event.target.files && event.target.files[0]){
            const i = event.target.files[0];
            setImage(i)
            setCreateObjectURL(URL.createObjectURL(i));
        }
    };
    
    const [data, setData] = useState({
        platform: "",
        position: "",
        url: "",
        status: "",
        start_date: "",
        end_date: "",
        title_id: "",
        summary_id: "",
        caption_id: "",
    })

    if(id && loop){
        axios.get(`https://backlink-service.herokuapp.com/api/v2/backlinks/${id}`)
        .then((res) => {
            const newData = {...data}
            console.log(res.data.data)
            newData['platform'] = res.data.data.platform
            newData['position'] = res.data.data.position
            newData['url'] = res.data.data.url
            newData['status'] = res.data.data.status
            newData['start_date'] = res.data.data.start_date
            newData['end_date'] = res.data.data.end_date
            newData['title_id'] = res.data.data.backlink_translations[0].title
            newData['summary_id'] = res.data.data.backlink_translations[0].summary
            newData['caption_id'] = res.data.data.backlink_translations[0].caption
            setData(newData)
            console.log(newData)
        })
        .catch((err) => {
            console.log(err.data);
        })
        setLoop(false)
    }

    function handle(e) {
        const newData = {...data}
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)
    }

    function submit(e) {
        e.preventDefault();
        if(id){
            axios.put(`https://backlink-service.herokuapp.com/api/v2/backlinks/${id}`, {
            platform: data.platform,
            position: data.position,
            url: data.url,
            status: data.status,
            start_date: data.start_date,
            end_date: data.end_date,
            title_id: data.title_id,
            summary_id: data.summary_id,
            caption_id: data.caption_id,
        })
        }else{
		axios.post('https://backlink-service.herokuapp.com/api/v2/backlinks', {
            platform: data.platform,
            position: data.position,
            url: data.url,
            status: data.status,
            start_date: data.start_date,
            end_date: data.end_date,
            title_id: data.title_id,
            summary_id: data.summary_id,
            caption_id: data.caption_id,
        })
        }
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Save',
            denyButtonText: `Don't save`,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              Swal.fire('Saved!', '', 'success')
              .then(res => {
                console.log('posted', res);
                router.push('.')
            })
            } else if (result.isDenied) {
              Swal.fire('Changes are not saved', '', 'info')
            }
          })
            
	}
    
    function handleEdit() {
        setEdit(true);
    }
    
    return (
    <>
        <div className="w-full p-5 pb-0">
            <nav className="flex mb-7" aria-label="Breadcrumb">
                <ol className="inline-flex items-center space-x-1 md:space-x-3">
                    <li className="inline-flex items-center">
                        <a href="#" className="inline-flex items-center text-sm font-medium text-gray-900">
                            Publication
                        </a>
                    </li>
                    <li>
                        <div className="flex items-center">
                            <iconify-icon inline icon="mdi:slash-forward" className="text-gray-400"></iconify-icon>
                            <a href="/" className="ml-1 text-sm font-medium hover:text-gray-900">Backlink</a>
                        </div>
                    </li>
                    <li aria-current="page">
                        <div className="flex items-center">
                            <iconify-icon inline icon="mdi:slash-forward" className="text-gray-400"></iconify-icon>
                            <span className="ml-1 text-sm font-medium text-red md:ml-2">Create Backlink</span>
                        </div>
                    </li>
                </ol>
            </nav>
        </div>
        <form onSubmit={submit}>
            <div className="w-full flex flex-wrap"> 
                <div className="w-full lg:w-2/3 p-5 pt-0">
                    <div className="container">                        
                        <div className="flex items-center mb-5">
                            <label className="w-20 text-black text-sm font-medium inline-block text-left mr-4">Platform<span className="text-red">*</span></label>
                            <select onChange={(e) => handle(e)} id="platform" value={data.platform} className="w-full border pl-3 py-1 rounded-md text-sm font-medium" required>
                                <option disabled selected defaultValue="Choose Platform">Choose Platform</option>
                                <option value="Bisnis.com">Bisnis.com</option>
                                <option value="Insta Indonesia">Insta Indonesia</option>
                                <option value="HypeAbis">HypeAbis</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Facebook">Facebook</option>
                            </select>
                        </div>

                        <div className="flex items-center mb-10">
                            <label className="w-20 text-black text-sm font-medium inline-block text-left mr-4">Position<span className="text-red">*</span></label>
                            <select onChange={(e) => handle(e)} id="position" value={data.position} className="w-full border pl-3 py-1 rounded-md text-sm font-medium" required>
                                <option disabled selected defaultValue="Choose Position">Choose Position</option>
                                <option value="Top Ad">Top Ad</option>
                                <option value="Below Ad">Below Ad</option>
                            </select>
                        </div>
                        <Language />
                        <div className="mb-6 pt-5 flex flex-col items-start">
                            <label className="block mb-2 text-sm font-medium text-gray-900 ">{currentLanguage[language].add_image}<span className="text-red">*</span></label>
                            <div className="inline-block flex flex-row w-full">
                                <input type="text" onChange={(e) => handle(e)} id="image" value={data.image} className="bg-grayy text-grayy2 text-sm w-2/3 rounded-md block p-2.5" placeholder="https://dataindonesia.id/media-images/1640665790292_97_Lansia.png" />
                                <div className="inline-block flex flex-row w-1/3 items-center">
                                    <input type="radio" value="" name="default-radio" className="w-4 h-4 ml-5 inline-block align-center text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600" />
                                    <label className="ml-2  text-sm font-medium text-gray-900 dark:text-black">{currentLanguage[language].set_thumbnail}</label>
                                </div>
                                <div className="inline-block flex grid justify-end w-1/3 items-end">
                                    <button className="py-2 px-8 bg-red text-white text-sm font-bold rounded">{currentLanguage[language].remove}</button>
                                </div>
                            </div>
                            <input type="" onChange={(e) => handle(e)} id="caption_id" value={data.caption_id} className="mt-5 bg-white border border-gray-300 text-grayy2 text-sm w-full rounded-md block p-2.5" placeholder={currentLanguage[language].add_caption} />
                        </div>
                        <img src={createObjectURL} className="w-1/4 pb-5"/>

                        <div className="mx-auto max-w-full">
                            <div className="w-full flex flex-col items-center py-12 px-6 rounded-sm border-2 bg-white shadow-lg">

                                <label className="bg-white px-4 h-9 inline-flex items-center rounded-md border border-red shadow-sm text-sm font-medium text-red focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                    {currentLanguage[language].upload_from}
                                    <input type="file" multiple className="sr-only" 
                                        onChange={uploadToClient}
                                        // onClick={uploadToServer}
                                    />
                                </label>

                                <p className="text-xs text-bold mt-4">{currentLanguage[language].reccomend_size} : 1900px x 998px </p>
                            </div>
                        </div>

                        <div className="flex items-center mb-5">
                            <label className="w-20 text-black text-sm font-medium inline-block text-left ml-2 mt-5">{currentLanguage[language].title}<span className="text-red">*</span></label>
                            <label className="w-full text-black text-sm font-medium inline-block text-right ml-3 mt-5">64 char remaining 0</label>
                        </div>
                        <input type="text" onChange={(e) => handle(e)} id="title_id" value={data.title_id} className="mt-5 ml-2 bg-white border border-gray-300 text-sm w-full rounded-md block p-2"/>
                        
                        <div className="flex items-center mb-5">
                            <label className="w-20 text-black text-sm font-medium inline-block text-left ml-2 mt-5">{currentLanguage[language].summary}<span className="text-red">*</span></label>
                            <label className="w-full text-black text-sm font-medium inline-block text-right ml-3 mt-5">300 char remaining 0</label>
                        </div>
                        <input type="text" onChange={(e) => handle(e)} id="summary_id" value={data.summary_id} className="mt-5 ml-2 bg-white border border-gray-300 text-sm w-full rounded-md block p-8"/>
                    </div>
                </div>
                <div className="w-full lg:w-1/3 px-5">
                    <div className="w-full flex flex-col rounded-sm border-2 py-6 bg-white shadow-lg">
                        <div className="font-semibold px-4">    
                            <p>
                                Publication Status
                            </p>
                        </div>
                        <div className="font-normal px-4 pt-4">    
                            <label className="w-20 text-black text-sm font-medium inline-block text-left mr-4">
                                Start Date<span className="text-red">*</span>
                            </label>
                            <input type="datetime-local" onChange={(e) => handle(e)} id="start_date" value={data.start_date} className="mt-1 bg-white border border-gray-300 text-grayy2 text-sm w-full rounded-md block p-2"/>
                        </div>
                        <div className="font-normal px-4 pt-4">    
                            <label className="w-20 text-black text-sm font-medium inline-block text-left mr-4">
                                End Date<span className="text-red">*</span>
                            </label>
                            <input type="datetime-local" onChange={(e) => handle(e)} id="end_date" value={data.end_date} className="mt-1 bg-white border border-gray-300 text-grayy2 text-sm w-full rounded-md block p-2" />
                        </div>
                        <div className="font-normal px-4 pt-4">    
                            <label className="w-20 text-black text-sm font-medium inline-block text-left mr-4">
                                Status
                            </label>
                            {/* <input type="" id="" className="mt-1 bg-white border border-gray-300 text-grayy2 text-sm w-full rounded-md block p-2" placeholder="Draft" /> */}
                            <select onChange={(e) => handle(e)} id="status" value={data.status} className="w-full border mt-1 p-2 rounded-md text-sm font-medium text-grayy2">
                                <option disabled selected defaultValue="Choose Position">Choose Status</option>
                                <option value="Draft">Draft</option>
                                <option value="Submitted">Submited</option>
                            </select>
                        </div>
                    </div>

                    <div className="mt-9 w-full flex flex-col rounded-sm border-2 py-6 bg-white shadow-lg">
                        <div className="flex font-normal text-sm px-4">    
                            <div className="w-40">
                                <p>
                                    Article Type :
                                </p>
                            </div>
                            <div>
                                <p className="font-bold">
                                    -
                                </p>
                            </div>
                        </div>
                        <div className="flex font-normal text-sm px-4 pt-4">    
                            <div className="w-40">
                                <p>
                                    Public Schedule :
                                </p>
                            </div>
                            <div>
                                <p className="font-bold">
                                    Not Set
                                </p>
                            </div>
                        </div>
                        <div className="flex font-normal text-sm px-4 pt-4">    
                            <div className="w-40">
                                <p>
                                    Status :
                                </p>
                            </div>
                            <div>
                                <p className="font-bold">
                                    Draft
                                </p>
                            </div>
                        </div>
                        <div className="flex-auto font-normal text-sm px-4 pt-4 mt-2">  
                            <button className="w-full py-2 px-8 bg-white text-red font-bold rounded outline outline-red outline-1">Translate</button>
                        </div>
                        <div className="flex font-normal text-sm px-4 pt-4">  
                            <button type="submit" className="focus:cursor-wait cursor:pointer w-full py-2 px-8 bg-red text-white font-bold rounded">Save</button>
                        </div>
                    </div>
                </div>
                <div className="w-full lg:w-2/3 p-5 pt-0">
                    <div className="container">
                        <div className="flex items-center mb-5">
                            <label className="w-full text-black text-sm font-medium inline-block text-left ml-2 mt-5 lg:mt-0">{currentLanguage[language].article} URL<span className="text-red">*</span></label>
                            <span href="#" onClick={handleEdit} style={{cursor: 'pointer'}} className="w-full text-red text-sm font-medium inline-block text-right ml-3 mt-5 lg:mt-0">Edit</span>
                        </div>
                        <input type="text" onChange={(e) => handle(e)} id="url" value={data.url} className="mt-5 lg:mt-0 ml-2 bg-white border border-gray-300 text-sm w-full rounded-md block p-2.5 " placeholder="htpss://"/>
                    </div>
                </div>
                { edit ? <Edit setEdit={setEdit} /> : ''}      
            </div>
        </form>
    </>
  )
}