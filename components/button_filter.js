import React from "react";
import {useRouter} from 'next/router';
import { useState } from "react"

export const Filter = ({setFilter}) => {

    const router = useRouter();
    const [reload, setReload] = useState(false);
    const {query: {limit}} = router

    function handleFilter() {
        setFilter(false);
    }

    const [data, setData] = useState({
        platform: "",
        position: "",
        status: "",
    })

    function handle(e) {
        const newData = {...data}
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)
    }

    function filterPosition(e) {
        e.preventDefault()
        router.push({
            pathname: './',
            query: {
                position:data.position,
                limit:limit
            }
        }).then(() => router.reload())
    }

    function filterPlatform(e) {
        e.preventDefault()
        router.push({
            pathname: './',
            query: {
                platform:data.platform,
                limit:limit
            }
        }).then(() => router.reload())
    }

    function filterStatus(e) {
        e.preventDefault()
        router.push({
            pathname: './',
            query: {
                status:data.status,
                limit:limit
            }
        }).then(() => router.reload())
    }

    return (
    <div className="lg:w-full lg:flex lg:flex-wrap">
        <div className="w-full lg:w-1/3 p-5 pl-0">
            <div className="w-full container">
                <label className="text-black text-sm font-medium inline-block text-left mr-1 pl-3 lg:pl-0">By Platform</label>
                <div className="flex flex-row">
                    <select onChange={(e) => handle(e)} id="platform" className="w-36 border p-2 rounded-md text-sm text-black font-medium" required>
                        <option disabled selected defaultValue="Choose Platform">Choose Platform</option>
                        <option value="Bisnis.com">Bisnis.com</option>
                        <option value="Insta Indonesia">Insta Indonesia</option>
                        <option value="HypeAbis">HypeAbis</option>
                        <option value="Twitter">Twitter</option>
                        <option value="Facebook">Facebook</option>
                    </select>
                    <button type="submit" onClick={filterPlatform} className="text-white rounded-md text-sm ml-2 mr-2 px-4 py-2 bg-red">Choose</button>
                </div>
            </div>
        </div>
        <div className="w-full lg:w-1/3 p-5">
            <div className="w-full container">
                <label className="text-black text-sm font-medium inline-block text-left mr-1 pl-3 lg:pl-0">By Position</label>
                <div className="flex flex-row lg:pl-0">
                    <select onChange={(e) => handle(e)} id="position" className="w-36 border p-2 rounded-md text-sm text-black font-medium" required>
                        <option disabled selected defaultValue="Choose Position">Choose Position</option>
                        <option value="Top Ad">Top Ad</option>
                        <option value="Below Ad">Below Ad</option>
                    </select>
                    <button type="submit" onClick={filterPosition} className="text-white rounded-md text-sm ml-2 mr-2 px-4 py-2 bg-red">Choose</button>
                </div>
            </div>
        </div>
        <div className="w-full lg:w-1/3 p-5">
            <div className="w-full container">
                <label className="text-black text-sm font-medium inline-block text-left mr-1 pl-3 lg:pl-0">By Status</label>
                <div className="flex flex-row pb-5 lg:pl-0">
                    <select onChange={(e) => handle(e)} id="status" className="w-36 border p-2 rounded-md text-sm text-black font-medium" required>
                        <option disabled selected defaultValue="Choose Status">Choose Status</option>
                        <option value="Draft">Draft</option>
                        <option value="Submitted">Submitted</option>
                    </select>
                    <button type="submit" onClick={filterStatus} className="text-white rounded-md text-sm ml-2 mr-2 px-4 py-2 bg-red">Choose</button>
                </div>
            </div>
        </div>
    </div>
    )
}